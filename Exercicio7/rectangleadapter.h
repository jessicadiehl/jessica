#ifndef RECTANGLEADAPTER_H
#define RECTANGLEADAPTER_H
#include <legacyrectangle.h>
#include <rectangle.h>
#include <iostream>

using namespace std;

class RectangleAdapter: public Rectangle, private  LegacyRectangle
{
public:
    RectangleAdapter(int x, int y, int w, int h):
    LegacyRectangle(x, y, x+w, y+h){
        cout<<"RectangleAdapter(x, y, x+w, y+h)";
    }

    void draw();

};

#endif // RECTANGLEADAPTER_H
