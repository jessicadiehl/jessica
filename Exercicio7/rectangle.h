#ifndef RECTANGLE_H
#define RECTANGLE_H


class Rectangle
{
public:
    Rectangle();
    virtual void draw()=0;
};

#endif // RECTANGLE_H
