#include <QCoreApplication>
#include <rectangle.h>
#include <rectangleadapter.h>
#include <legacyrectangle.h>
#include <iostream>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    int x=20, y=50, w=300, h=50;

    Rectangle *r= new RectangleAdapter(x,y,w,h);
    r->draw(); //aponta pro metodo draw da adapter

    return a.exec();
}
