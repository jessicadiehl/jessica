#ifndef VALORMAXIMO_H
#define VALORMAXIMO_H
#include <iostream>

using namespace std;

class ValorMaximo
{
public:
    ValorMaximo();
    float FindMax(float n[]);
    float getMax();

 private:
    float maximo;
};

#endif // VALORMAXIMO_H
