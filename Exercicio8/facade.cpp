#include "facade.h"

Facade::Facade()
{

}

Facade::Facade(float n[])
{
   VMed.CalculoMedia(n);
   VMax.FindMax(n);
   VMin.FindMin(n);
}

float Facade::getFacade()
{
   VMax.getMax();
   VMed.getMedia();
   VMin.getMin();
}
