#include <QCoreApplication>
#include <facade.h>
#include <iostream>

using namespace std;
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    float v[10]={10,9,9,8,6,5,4,3,2,1.5};

    Facade f(v);
    f.getFacade();

    return a.exec();
}
