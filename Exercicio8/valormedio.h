#ifndef VALORMEDIO_H
#define VALORMEDIO_H
#include <iostream>

using namespace std;
class ValorMedio
{
public:
    ValorMedio();
    float CalculoMedia(float n[]);
    float getMedia();

private:

    float media;
};

#endif // VALORMEDIO_H
