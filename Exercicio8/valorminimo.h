#ifndef VALORMINIMO_H
#define VALORMINIMO_H
#include <iostream>

using namespace std;

class ValorMinimo
{
public:
    ValorMinimo();
    float FindMin(float n[]);
    float getMin();

private:
    float minimo;

};

#endif // VALORMINIMO_H
