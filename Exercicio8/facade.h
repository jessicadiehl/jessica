#ifndef FACADE_H
#define FACADE_H
#include <valormaximo.h>
#include <valormedio.h>
#include <valorminimo.h>


class Facade
{
public:
    Facade();
    Facade(float n[]);
    float getFacade();

private:

    ValorMaximo VMax;
    ValorMedio VMed;
    ValorMinimo VMin;
};

#endif // FACADE_H
