#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QString>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
 //   i-0;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btn_busca_clicked()
{
   QString str=ui->texto->toHtml();
    if(str.size()!=0)
   {
       ui->texto->clear();
       ui->texto->setHtml(str);
   }

   QString busca = ui->palavra->text(); //declaracao do objeto com a palavra a ser localizada
   QString texto=ui->texto->toPlainText(); //declaracao do objeto com o texto onde deve ser feita a pesquisa


   Qt::CaseSensitivity ignorar;
   if(ui->checkBox->isChecked()) ignorar=Qt::CaseInsensitive;
   else ignorar = Qt::CaseSensitive;

   int j=0;

    if(texto.contains(busca,ignorar)){
        j=texto.count(busca, ignorar);
        str=ui->texto->toPlainText();
        texto.replace(busca, QString("<font color=\"#FF0000\">" + busca + "</font>"), ignorar);}
    else ui->num_palavra->setText(QString("%1").arg(j));
    ui->texto->clear();
    ui->texto->setHtml(texto);
    ui->num_palavra->clear();
    ui->num_palavra->insert(QString::number(j));

}

